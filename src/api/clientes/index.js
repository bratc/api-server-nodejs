'use strict';

var express = require('express');
var controller = require('./controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/search/', controller.searchByDocument);
router.put('/:id', controller.update);
router.post('/', controller.save);
router.delete('/:id', controller.destroy);

module.exports = router;
