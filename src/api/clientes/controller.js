const { Clientes } = require('../../sqldb');

  exports.index = async (req, res) => {
    try {
      const result = await Clientes.findAll();
      res.status(200).send(result);
    } catch (err) {
      console.log('err ', err)
    }
  }
  
  exports.update = async (req, res) => {
    try {
      let cliente = req.body;
      let result = await Clientes.update(cliente, { where: { _id : req.params.id }});
      res.status(200).send(result);
    } catch (err) {
      console.log('err ', err)
    }
  }
  
  
  exports.show = async (req, res) => {
    var clienteId = req.params.id;
    try {
      const result = await Clientes.find({
        where: {
          id: clienteId
        }
      })
      res.status(200).send(result);
    } catch (err) {
      console.log('err ', err)
      res.status(400).send(err);
    }
  }
  exports.save = async (req, res) => {
    try {
      var cliente = req.body;
      var clienteFound = await Clientes.findAll({
        where: {
          email: cliente.document_number
        }
      });
      if (clienteFound.length > 0) {
          res.status(400).send({
            message: 'El numero de documento ya se encuentra registrado'
          });
        } else {
          await Clientes.create(cliente).then(cliente => {
            res.status(200).send(cliente);
          });
        }
      
  
    } catch (err) {
      console.log(err);
      res.status(400).send({message: 'Error al crear el usuario'});
    }
  }
 
  
  exports.searchByDocument = async (req, res) => {
    try{
      const result = await Clientes.find({
            where: {
              document_number: req.query.document_number
            }
  
    });
      res.status(200).send(result);
    }catch(err ){
      res.status(400).send(err);
      console.log(err)
    }
  }

  exports.destroy = async (req, res) => {
    try{
      const result = await Clientes.destroy({
            where: {
              _id: req.query._id
            }
  
    });
      res.status(200).send(result);
    }catch(err ){
      res.status(400).send(err);
      console.log(err)
    }
  }
  