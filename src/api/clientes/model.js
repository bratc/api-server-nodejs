module.exports = (sequelize, DataTypes) => {
    const Clientes = sequelize.define('Clientes', {
        _id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        document_number: {
            type: DataTypes.STRING(20),
            allowNull: true
        },
        fingerprint: {
            type: DataTypes.BLOB(),
            allowNull: true
        },
        first_name: {
            type: DataTypes.STRING(60),
            allowNull: true
        },
        last_name: {
            type: DataTypes.STRING(60),
            allowNull: true
        },
        date_register: {
            type: DataTypes.DATE(),
            allowNull: true
        }
      });

  

    return Clientes;
}
