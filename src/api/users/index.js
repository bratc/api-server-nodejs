const express = require('express');
const controller = require('./controller');
const auth = require('../../auth/auth.service');
const router = express.Router();

router.get('/', controller.findAll);
router.get('/:id',auth.hasRole([2,3]),controller.show);
router.get('/roles/:id', controller.findAllByRol);
router.post('/', controller.saveUser);

router.put('/:id',controller.update);
module.exports = router;
