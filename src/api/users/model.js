const encryptPassword = require('encrypt-password')
module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('Users', {
        _id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
          },
        first_name: DataTypes.STRING(60),
        last_name: DataTypes.STRING(60),
        username: {
          type: DataTypes.STRING(20),
          unique: {
            msg: 'The username is unique.'
          }
        },
        email: {
          type: DataTypes.STRING,
          unique: {
            msg: 'The specified email address is already in use.'
          },
          validate: {
            isEmail: true,
            }
        },
        password: {
          type: DataTypes.STRING,
          validate: {
            notEmpty: true
          }
        },
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: true
        }


    },{
        hooks: {
            beforeCreate: function(user) {
                user.password  = encryptPassword(user.password, 'signatrue')
;                //sha512(user.password).toString('hex');
            }
        }
    });

    User.associate = (models) => {


        User.belongsToMany(models.Rol, {
                      through: 'users_has_roles',
                      as: 'roles',
                      foreignKey: {
                        name: 'idUser',
                        allowNull: true
                      },
                      otherKey: {
                        name: 'idRol',
                        allowNull: true

                      }
                  });
      
      

    }
    return User;
}
