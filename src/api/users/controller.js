const {
  User
} = require('../../sqldb');
const {
  Rol,
  Sequelize
} = require('../../sqldb');
exports.findAll = async (req, res) => {
  try {
    const result = await User.findAll({
          include: [{
          model: Rol,
          as: 'roles',
          }
      ]
    });
    res.status(200).send(result);
  } catch (err) {
    console.log('err ', err)
  }
}

exports.update = async (req, res) => {
  try {
    let user = req.body;
    let result = await User.update(user, { where: { _id : req.params.id }});
    res.status(200).send(result);
  } catch (err) {
    console.log('err ', err)
  }
}


exports.show = async (req, res) => {
  var userId = req.params.id;
  try {
    const result = await User.find({
      where: {
        id: userId
      }
    })
    res.status(200).send(result);
  } catch (err) {
    console.log('err ', err)
    res.status(400).send(err);
  }
}
exports.saveUser = async (req, res) => {
  try {
    var user = req.body;
    var userFound = await User.findAll({
      where: {
        email: user.email
      }
    });
    if (userFound.length > 0) {
      res.status(400).send({
        message: 'El email ya se encuentra registrado'
      });
    }  else {
        await User.create(user).then(user => {
          res.status(200).send(user);
        });
      
    }

  } catch (err) {
    console.log(err);
    res.status(400).send({message: 'Error al crear el usuario'});
  }
}
exports.findAllByRol = async (req, res) => {
  try {
    const result = await User.findAll({
          include: [{
          model: Rol,
          as: 'roles',
          where: {
              id: req.params.id
          }
      }]
  });
    res.status(200).send(result);
  } catch (err) {
    console.log('err ', err)
    res.status(400).send(err);
  }
}

exports.buscarCliente = async (req, res) => {
  try{
    const result = await User.find({
          where: {
            documento: req.query.documento
          }

  });
    res.status(200).send(result);
  }catch(err ){
    res.status(400).send(err);
    console.log(err)
  }
}
