module.exports = (sequelize, DataTypes) => {
    const Rol = sequelize.define('Roles', {
        _id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        rol: {
            type: DataTypes.STRING(45),
            allowNull: false
        }});

    Rol.associate = (models) => {

      Rol.belongsToMany(models.User,
          {
              through: 'users_has_roles',
              as: 'roles',
              foreignKey: {
                name: 'idRol',
                allowNull: false
              },
              otherKey: {
                  name: 'idUser',
                  allowNull: false
              }
          });
      }

    return Rol;
}
