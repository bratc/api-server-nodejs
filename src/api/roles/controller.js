'use strict';
const { Rol } = require('../../sqldb');

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    return entity.updateAttributes(updates)
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Rols
exports.index = async(req, res)=> {
  let roles= await Rol.findAll()
    .then(respondWithResult(res))
    .catch(handleError(res));
    return roles;
}

// Gets a single Rol from the DB
exports.show = async(req, res)=> {
  let rol= await Rol.find({
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));

return rol;
}

// Creates a new Rol in the DB
exports.create = async(req, res)=> {
  let rol = await Rol.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));

    return rol;
}

// Updates an existing Rol in the DB
exports.update= async(req, res)=> {
  if (req.body.id) {
    delete req.body.id;
  }
  let rol= await Rol.find({
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
    return rol;
}

// Deletes a Rol from the DB
exports.destroy= async(req, res)=> {
  let rol= await Rol.find({
    where: {
      id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));

    return rol;
}
