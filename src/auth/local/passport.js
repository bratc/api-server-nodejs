const passport  = require('passport');
const Strategy  = require('passport-local');
const LocalStrategy = Strategy;
const encryptPassword = require('encrypt-password')


const {Rol} = require('../../sqldb');
function localAuthenticate(User, username, password, done) {
  User.find({
    where: {
      username: username.toLowerCase()
    },
    include: [
    {
      model: Rol,
      as: 'roles'
    }
  ]
  })
    .then(user => {
      if(!user) {
        return done(null, false, {
          message: 'This username is not registered.'
        });
      }
      console.log("USER PASS ",encryptPassword(password, 'signatrue') );
      var sendPass = encryptPassword(password, 'signatrue') 
      if (password) {
        if (user.password === sendPass.toString()) {

          return done(null, user);
        } else {
          return done(null, false, {
            message: 'The password is incorrect!.'
          });
        }
      } else {
        return done(authError);
      }
    })
    .catch(err => done(err));
}

exports.setup = (User, config)=> {
  passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password' // this is the virtual field on the model
  }, function(username, password, done) {
    return localAuthenticate(User, username, password, done);
  }));
}
