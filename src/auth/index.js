'use strict';

const express = require('express');
const passport = require('passport');
const { User } = require('../sqldb');
const config = require('../config');


// Passport Configuration
require('./local/passport').setup(User,config);

var router = express.Router();

router.use('/local', require('./local'));

module.exports= router;
