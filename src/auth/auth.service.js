'use strict';

const passport = require('passport');
const config = require('../config');
const jwt = require('jsonwebtoken');
const compose = require('composable-middleware');
const {User} = require('../sqldb');
const db = require('../sqldb');
const controller = require('./../api/users/controller');
const expressJwt = require('express-jwt');

var validateJwt = expressJwt({
    secret: config.SECRET_TOKEN
});

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
exports.isAuthenticated = ()=> {
    return compose()
        // Validate jwt
        .use(function(req, res, next) {
            // allow access_token to be passed through query parameter as well
            if (req.query && req.query.hasOwnProperty('access_token')) {
                req.headers.authorization = 'Bearer ' + req.query.access_token;
            }
            validateJwt(req, res, next);
        })
        // Attach user to request
        .use(function(req, res, next) {
            User.find({
                    where: {
                        id: req.user.id
                    }
                })
                .then(user => {
                    if (!user) {
                        return res.status(401).end();
                    }
                    req.user = user;
                    next();
                })
                .catch(err => next(err));
        });
}

/**
 * Checks if the user role meets the minimum requirements of the route
 */
exports.hasRole =(roleRequired)=> {
    var list = [];
    list = roleRequired;
    var respuesta = false;
    if (!roleRequired) {
        throw new Error('Debe tener un rol.');
    }
    return compose()
        .use(this.isAuthenticated())
        .use(function meetsRequirements(req, res, next) {
            db.sequelize.query(
                    "SELECT r.id FROM users u " +
                    "INNER JOIN users_has_roles ru INNER JOIN roles r " +
                    "WHERE u.id = ru.idUser AND ru.idRol = r.id " +
                    "AND u.id = " + req.user.id, {
                        type: db.sequelize.QueryTypes.SELECT
                    })
                .then(function(roles) {
                    for (var i = 0; i < roles.length; i++) {
                        for (var j = 0; j < list.length; j++) {
                            if (list[j] === roles[i].id) {
                                respuesta = true;
                            }
                        }
                    }
                    return respuesta;
                })
                .then(function() {
                    if (respuesta) {
                        next();
                    } else {
                        res.status(403).send('Error no tiene el rol requerido para acceder a esta ruta');
                    }
                })
                .catch(function(err) {
                    return err;
                });
        });
}
/**
 * Returns a jwt token signed by the app secret
 */
exports.signToken = (id, first_name, roles)=> {
  var rolesList=[];
  for (var i = 0; i < roles.length; i++) {
    rolesList.push(roles[i].id)
  }
    return jwt.sign({
        id: id,
        user: first_name,
        roles:`[${rolesList.toString()}]`
    }, config.SECRET_TOKEN, {
        expiresIn: 60 * 60 * 5
    });
}

/**
 * Set token cookie directly for oAuth strategies
 */
exports.setTokenCookie =(req, res)=> {
    if (!req.user) {
        return res.status(404).send('It looks like you aren\'t logged in, please try again.');
    }
    var token = signToken(req.user.id, req.user.user,req.user.roles);
    res.cookie('token', token);
    res.redirect('/');
}
