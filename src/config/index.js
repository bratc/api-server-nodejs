module.exports = {
      ENV: process.env.ENV || 'development',
      PORT: process.env.PORT || 3000,
      SECRET_TOKEN: process.env.SECRET_TOKEN || 'servicios_app',
      DATABASE: {
          URI: process.env.URI_DB || 'mysql://root:A1s2d3f4g5.@localhost:3306/bd_api',
          options: {
              host: process.env.HOST || 'localhost',
              dialect: process.env.DIALECT_DB || 'mysql',
              timezone: 'America/Bogota',
              define: {
                  timestamps: false,
              },
          },
      },
  }
