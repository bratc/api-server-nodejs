const express = require('express');
const bodyParser = require('body-parser');
var path = require('path');
const cors = require('cors');

const user = require('./api/users');
const clientes = require('./api/clientes');
const rol = require('./api/roles');

const auth = require('./auth');
const app = express();
const  http  = require('http');


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
/*app.use(function(req, res, next) {
 // res.header("Access-Control-Allow-Origin", '*');
 // res.header("Access-Control-Allow-Credentials", true);
 // res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
 // res.header("Access-Control-Allow-Headers",
'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json,Authorization');
  next();
});
*/
app.use(cors());

app.use('/api/users', user);
app.use('/api/roles', rol);
app.use('/api/clientes',clientes);


// All undefined asset or api routes should return a 404

app.route('/:url(api|auth|components|app|bower_components)/*')
        .get((req, res) => {
          res.sendFile(path.resolve("./client/" + '404.html'));
        });

// All other routes should redirect to the index.html

// Assets static files
app.use('/assets', express.static(path.join(__dirname, '../client/assets')));

app.route('/*')
      .get((req, res) => {
            res.sendFile(path.resolve("./client/" + 'index.html'));
          });



app.use('/api/auth', auth);
var server = http.createServer(app);
/*

const io = require('socket.io')(server);
io.on('connection', (socket)=> {
  console.log('Un User se ha conectado');
  socket.on('enviarMensaje', function(mensaje) {
    console.log("MESAGE ",mensaje);
    io.emit('enviarMensaje', mensaje);
  });

  });
*/
/*
io.on('connection', (socket)=> {
  socket.on('enviarMensaje', function(mensaje) {
      console.log("MESAGE ",mensaje);
  	 io.emit('enviarMensaje', mensaje);
  });
});*/
module.exports = server;
