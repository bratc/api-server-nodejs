const Sequelize = require('sequelize');
const { DATABASE } = require('../config');

const db = {
    Sequelize,
    sequelize: new Sequelize(
        DATABASE.URI, DATABASE.options
    ),
  };

db.User = db.sequelize.import('../api/users/model');
db.Clientes = db.sequelize.import('../api/clientes/model');
db.Rol = db.sequelize.import('../api/roles/model');

Object.keys(db).forEach((modelName) => {
    if ('associate' in db[modelName]) {
      db[modelName].associate(db);
    }
  });

module.exports = db;
