const app = require('./src/app');
const { PORT } = require('./src/config');
const sqldb = require('./src/sqldb');
const { log } = console;

function resultOk() {
    log('Configuracion de sequelize OK');
    app.listen(PORT, () => {
        log(`Servidor corriendo en el puerto ${PORT}`);
    })
}

function resultErr(err) {
    console.log(`Hubo un error ${err}`);
}

sqldb.sequelize
    .authenticate()
    .then(() => resultOk())
    .catch(err => resultErr(err));
